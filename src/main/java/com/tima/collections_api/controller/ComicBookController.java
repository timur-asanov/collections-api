package com.tima.collections_api.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tima.collections_api.model.APICountResponse;
import com.tima.collections_api.model.APIPageRequest;
import com.tima.collections_api.model.ComicResponse;
import com.tima.collections_api.model.ComicsResponse;
import com.tima.collections_api.model.Constants;
import com.tima.collections_api.persistence.model.ComicBook;
import com.tima.collections_api.persistence.repository.ComicBookRepository;

@RestController
@Validated
public class ComicBookController extends ItemController<ComicBook> {
	
	private static Logger logger = LoggerFactory.getLogger(ComicBookController.class);
	
	@Autowired
	private ComicBookRepository comicBookRepository;

	@PostMapping(value = "/comics", produces = "application/json", consumes = "application/json")
	@Override
	public ResponseEntity<ComicsResponse> getItems(@RequestBody APIPageRequest pageRequest) {
		logger.info("fetching comics page: " + pageRequest.getPageNumber() + " with page size: " + pageRequest.getPageSize());
		
		try {
			List<ComicBook> comics = comicBookRepository.findAll(PageRequest.of(pageRequest.getPageNumber() - 1, pageRequest.getPageSize()));
			
			return new ResponseEntity<ComicsResponse>(new ComicsResponse(comics), HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			return new ResponseEntity<ComicsResponse>(new ComicsResponse(Constants.GENERIC_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/comics/{id}", produces = "application/json")
	@Override
	public ResponseEntity<ComicResponse> getItem(@PathVariable String id) {
		logger.info("fetching comic: " + id + ":::" + (id == null ? "0" : id.length()));
		
		try {
			Optional<ComicBook> comic = comicBookRepository.findById(id);
			
			return new ResponseEntity<ComicResponse>(comic.isPresent() ? new ComicResponse(comic.get()) : new ComicResponse(), HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			return new ResponseEntity<ComicResponse>(new ComicResponse(Constants.GENERIC_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/count/comics", produces = "application/json")
	@Override
	public ResponseEntity<APICountResponse> getCount() {
		logger.info("fetching comics' count");
		
		try {
			long count = comicBookRepository.getTotalCount();
			
			return new ResponseEntity<APICountResponse>(new APICountResponse(count), HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			return new ResponseEntity<APICountResponse>(new APICountResponse(Constants.GENERIC_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/count-unique/comics", produces = "application/json")
	@Override
	public ResponseEntity<APICountResponse> getUniqueCount() {
		logger.info("fetching comics' unique count");
		
		try {
			long count = comicBookRepository.count();
			
			return new ResponseEntity<APICountResponse>(new APICountResponse(count), HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			return new ResponseEntity<APICountResponse>(new APICountResponse(Constants.GENERIC_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
