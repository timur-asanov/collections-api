package com.tima.collections_api.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.http.ResponseEntity;

import com.tima.collections_api.model.APICountResponse;
import com.tima.collections_api.model.APIPageRequest;
import com.tima.collections_api.model.APIResponse;
import com.tima.collections_api.model.Constants;

public abstract class ItemController<T> {

	public abstract ResponseEntity<? extends APIResponse> getItems(
		@Valid 
		APIPageRequest pageRequest
	);
	public abstract ResponseEntity<? extends APIResponse> getItem(
		@NotBlank(message = Constants.UUID_EMPTY_ERROR) 
		@Size(min = Constants.UUID_LENGTH, max = Constants.UUID_LENGTH, message = Constants.UUID_LENGTH_ERROR) 
		String id
	);
	public abstract ResponseEntity<APICountResponse> getCount();
	public abstract ResponseEntity<APICountResponse> getUniqueCount();
}
