package com.tima.collections_api.model;

public class APICountResponse extends APIResponse {

	private long count;
	
	public APICountResponse() {
		super();
	}
	
	public APICountResponse(String... errors) {
		super(errors);
	}
	
	public APICountResponse(long count) {
		super();
		
		this.count = count;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "CountResponse [count=" + count + "]";
	}
}
