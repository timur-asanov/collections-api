package com.tima.collections_api.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class APIPageRequest extends APIRequest {

	@Min(value = Constants.MIN_PAGE_NUMBER, message = Constants.PAGE_NUMBER_ERROR)
	private int pageNumber;
	
	@Min(value = Constants.MIN_PAGE_SIZE, message = Constants.PAGE_SIZE_ERROR)
	@Max(value = Constants.MAX_PAGE_SIZE, message = Constants.PAGE_SIZE_ERROR)
	private int pageSize;
	
	public APIPageRequest() {
		super();
	}

	public APIPageRequest(int pageNumber, int pageSize) {
		super();
		
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "PageRequest [pageNumber=" + pageNumber + ", pageSize=" + pageSize + "]";
	}
}
