package com.tima.collections_api.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class APIResponse {

	@JsonInclude(Include.NON_EMPTY)
	private List<String> errors;
	
	public APIResponse() {}
	
	public APIResponse(String... errors) {
		if (errors != null && errors.length > 0) {
			this.errors = new ArrayList<String>();
			this.errors.addAll(Arrays.asList(errors));
		}
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public void addError(String error) {
		if (this.errors == null) {
			this.errors = new ArrayList<String>();
		}
		
		this.errors.add(error);
	}

	@Override
	public String toString() {
		return "APIResponse [errors=" + errors + "]";
	}
}
