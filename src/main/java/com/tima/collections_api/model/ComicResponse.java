package com.tima.collections_api.model;

import com.tima.collections_api.persistence.model.ComicBook;

public class ComicResponse extends APIResponse {

	private ComicBook comic;
	
	public ComicResponse() {
		super();
	}
	
	public ComicResponse(String... errors) {
		super(errors);
	}

	public ComicResponse(ComicBook comic) {
		super();
		
		this.comic = comic;
	}

	public ComicBook getComic() {
		return comic;
	}

	public void setComic(ComicBook comic) {
		this.comic = comic;
	}

	@Override
	public String toString() {
		return "ComicResponse [comic=" + comic + ", getErrors()=" + getErrors() + "]";
	}
}
