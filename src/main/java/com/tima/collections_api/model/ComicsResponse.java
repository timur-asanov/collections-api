package com.tima.collections_api.model;

import java.util.List;

import com.tima.collections_api.persistence.model.ComicBook;

public class ComicsResponse extends APIResponse {

	private List<ComicBook> comics;
	
	public ComicsResponse() {}
	
	public ComicsResponse(String... errors) {
		super(errors);
	}

	public ComicsResponse(List<ComicBook> comics) {
		this.comics = comics;
	}

	public List<ComicBook> getComics() {
		return comics;
	}

	public void setComics(List<ComicBook> comics) {
		this.comics = comics;
	}

	@Override
	public String toString() {
		return "ComicsResponse [comics=" + comics + ", getErrors()=" + getErrors() + "]";
	}
}
