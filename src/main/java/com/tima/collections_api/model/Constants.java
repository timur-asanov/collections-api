package com.tima.collections_api.model;

public class Constants {

	public static final int MIN_PAGE_NUMBER = 1;
	public static final int MIN_PAGE_SIZE = 1;
	public static final int MAX_PAGE_SIZE = 100;
	public static final int UUID_LENGTH = 36;
	
	// TODO errors will be basically the same; need to prevent copy/paste
//	public static final String TEMPLATE_EMPTY_ERROR = "%s is required.";
//	public static final String TEMPLATE_EXACT_ERROR = "%s must be %s characters.";
//	public static final String TEMPLATE_MIN_ERROR = "%s must be larger than %s.";
//	public static final String TEMPLATE_RANGE_ERROR = "%s must be between %s and %s.";
//	public static final String PAGE_NUMBER_ERROR = "" + String.format(TEMPLATE_MIN_ERROR, "page number", MIN_PAGE_NUMBER);
//	public static final String PAGE_SIZE_ERROR = "" + String.format(TEMPLATE_RANGE_ERROR, "page size", MIN_PAGE_SIZE, MAX_PAGE_SIZE);
//	public static final String UUID_EMPTY_ERROR = "" + String.format(TEMPLATE_EMPTY_ERROR, "UUID");
//	public static final String UUID_LENGTH_ERROR = "" + String.format(TEMPLATE_EXACT_ERROR, "UUID", UUID_LENGTH);
	
	public static final String GENERIC_ERROR = "An unknown exception occurred.";
	public static final String GENERIC_400_ERROR = "The incoming message was not uderstood.";
	
	public static final String PAGE_NUMBER_ERROR = "page number must be larger than " + MIN_PAGE_NUMBER + ".";
	public static final String PAGE_SIZE_ERROR = "page size must be between " + MIN_PAGE_SIZE + " and " + MAX_PAGE_SIZE + ".";
	public static final String UUID_EMPTY_ERROR = "uuid is required.";
	public static final String UUID_LENGTH_ERROR = "uuid must be " + UUID_LENGTH + " characters.";
	
}
