package com.tima.collections_api.persistence.model;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class CollectedItem {
	
	private String id;
	
	@JsonIgnore
	@Transient
	private CollectedItemTypes type;

	public CollectedItem() {}
	
	public CollectedItem(CollectedItemTypes type) {
		this.type = type;
	}
	
	public CollectedItem(String id, CollectedItemTypes type) {
		this.id = id;
		this.type = type;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CollectedItemTypes getType() {
		return type;
	}

	public abstract String toString();
}
