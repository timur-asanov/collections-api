package com.tima.collections_api.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "comics")
public class ComicBook extends CollectedItem {
	
	private String name;
	private String series;
	private String publisher;
	
	@Column(name = "issue_number")
	private String issueNumber;
	
	@Column(name = "published_date")
	private String publishedDate;
	
	@Column(name = "owned_number")
	private int ownedNumber;
	
	public ComicBook() {
		super(CollectedItemTypes.COMIC_BOOK);
	}
	
	public ComicBook(String id, String name, String series, String publisher, String issueNumber, String publishedDate, int ownedNumber) {
		super(id, CollectedItemTypes.COMIC_BOOK);
		
		this.name = name;
		this.series = series;
		this.publisher = publisher;
		this.issueNumber = issueNumber;
		this.publishedDate = publishedDate;
		this.ownedNumber = ownedNumber;
	}

	@Id
	public String getId() {
		return super.getId();
	}
	
	public void setId(String id) {
		super.setId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(String issueNumber) {
		this.issueNumber = issueNumber;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public int getOwnedNumber() {
		return ownedNumber;
	}

	public void setOwnedNumber(int ownedNumber) {
		this.ownedNumber = ownedNumber;
	}
	
	@Override
	public String toString() {
		return "ComicBook [name=" + name + ", series=" + series + ", publisher=" + publisher + ", issueNumber="
				+ issueNumber + ", publishedDate=" + publishedDate + ", ownedNumber=" + ownedNumber + "]";
	}
}
