package com.tima.collections_api.persistence.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tima.collections_api.persistence.model.ComicBook;

@Repository
public interface ComicBookRepository extends CrudRepository<ComicBook, String> {

	public List<ComicBook> findAll(Pageable pageable);
	
	@Query("SELECT SUM(c.ownedNumber) FROM ComicBook c")
	public long getTotalCount();
}
