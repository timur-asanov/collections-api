package com.tima.collections_api.utils;

import java.io.FileInputStream;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public enum ApplicationProperties {

	DB_URL,
	DB_USER,
	DB_PWD;
	
	private final Class<?> STRING_CLAZZ = String.class;
	
	private String stringValue;
	
	private static final String FILE_SYSTEM_PROPERTY_NAME = "prop-file.location";
	
	private Map<String, Object> propertyMap;
	
	private ApplicationProperties() {
		this(true);
	}
	
	private ApplicationProperties(boolean required) {
		stringValue = null;
		
		if (propertyMap == null) {
			try (FileInputStream fis = new FileInputStream(System.getProperty(FILE_SYSTEM_PROPERTY_NAME))) {
				Yaml yaml = new Yaml();
				
				propertyMap = yaml.load(fis);
			} catch (Exception e) {
				throw new RuntimeException("An exception occured while trying to read the property file.", e);
			}
		}
		
		if (propertyMap != null) {
			Object value = propertyMap.get(toString().toLowerCase());
			
			if (value == null) {
				if (required == true) {
					throw new RuntimeException("Property " + toString() + " is required, but was not provided.");
				}
			} else {
				Class<?> valueClazz = value.getClass();
				
				if (STRING_CLAZZ.isAssignableFrom(valueClazz) == true) {
					stringValue = (String) value;
				} else {
					throw new RuntimeException("Property " + toString() + " is of unknown type.");
				}
			}
		}
	}
	
	public String getStringValue() {
		return this.stringValue;
	}
}
