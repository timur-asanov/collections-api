package com.tima.collections_api.utils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.tima.collections_api.model.APIResponse;
import com.tima.collections_api.model.Constants;

@ControllerAdvice
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler {
	
	private static Logger logger = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

	@ExceptionHandler(value = { ConstraintViolationException.class })
	protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException e, WebRequest request) {
		APIResponse response = new APIResponse();
		
		if (e != null && e.getConstraintViolations().size() > 0) {
			for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
				logger.error(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " + violation.getMessage());
				
				response.addError(violation.getMessage());
		    }
		} else {
			response.addError(Constants.GENERIC_400_ERROR);
		}
		
		return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.error(e.getMessage(), e);

		return new ResponseEntity<Object>(new APIResponse(Constants.GENERIC_400_ERROR), HttpStatus.BAD_REQUEST);
	}
}
