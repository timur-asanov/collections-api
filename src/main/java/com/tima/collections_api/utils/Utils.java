package com.tima.collections_api.utils;

public class Utils {
	
	private static final String CAMEL_CASE_REGEX = "([a-z])([A-Z])";
	private static final String CAMEL_CASE_REPLACEMENT = "$1_$2";

	public static String camelCaseToUnderscore(String input) {
		if (input != null) {
			return input.replaceAll(CAMEL_CASE_REGEX, CAMEL_CASE_REPLACEMENT).toLowerCase();
		}
		
		return input;
	}
}
