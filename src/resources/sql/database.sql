DROP TABLE `comics`;

CREATE TABLE `comics` (
    `id` varchar(36) NOT NULL,
    `name` varchar(200) NOT NULL,
    `series` varchar(200) NOT NULL,
    `publisher` varchar(50) NOT NULL,
    `issue_number` varchar(10) NOT NULL,
    `published_date` varchar(50) NOT NULL DEFAULT '0',
    `owned_number` int NOT NULL,
    PRIMARY KEY (`id`)
);